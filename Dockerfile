FROM node:alpine

WORKDIR /home/daniel/ProjectDocker

COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build


EXPOSE 3000

CMD ["npm", "run", "start"]