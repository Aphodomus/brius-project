const express = require('express');

const PORT = 3000;
const HOST = '0.0.0.0';

const app = express();

app.use(express.static(__dirname + '/public/src'));

app.get('/', (request, response) => {
    response.sendFile(__dirname + "/public/src/index.html");
})

app.get('/interns', (request, response) => {
    response.sendFile(__dirname + "/src/interns.html");
})

app.listen(PORT, HOST);